package net.a3do.app.ejercicio3;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button mediaButton;
    private boolean playing = false;
    private MediaPlayer mediaPlayer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mediaButton = findViewById(R.id.mediaButton);
        mediaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playing) {
                    mediaPlayer.stop();
                    mediaButton.setText(getString(R.string.play_text));
                    playing = false;
                } else {
                    mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.never_gonna_give_you_up);
                    mediaPlayer.start();
                    mediaButton.setText(getString(R.string.pause_text));
                    playing = true;
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null) {
            mediaPlayer.release();
        }
    }
}