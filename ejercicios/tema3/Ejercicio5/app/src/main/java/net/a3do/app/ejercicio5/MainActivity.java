package net.a3do.app.ejercicio5;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.widget.MediaController;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity {

    MediaController mediaController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mediaController = new MediaController(this); // Creamos un media controller

        final VideoView videoView = (VideoView) findViewById(R.id.myVideo); // Obtenemos el video view del XML

        // definimos los parámetros del videoView
        videoView.setMediaController(mediaController);
        videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.virus_video));
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                // Iniciamos el vídeo cuando se carge el widget
                mediaController.show(5000); // mostramos el media controller por 5 seg
                videoView.start();
            }
        });s
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // si tocamso la pantalla se vuelve a mostrar el media controller
        mediaController.show();
        return false;
    }
}