package com.afundaconfp;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static int calcularSumaDigitos(int numero) {
        int suma = 0;
        String numeroStr = Integer.toString(numero);
        System.out.print("Suma interna: ");
        for (int i = 0; i < numeroStr.length(); i++) {
            System.out.print(numeroStr.charAt(i));
            if (i != numeroStr.length() - 1) System.out.print(" + ");
            suma += Integer.parseInt("" + numeroStr.charAt(i));
        }
        System.out.println(" = " + suma);
        return suma;
    }

    public static void calcular(int[] a, int numx, int ultimaPosicionCalculada) {
//        System.out.println("ultimaPosicionCalculada:" + ultimaPosicionCalculada);
//        System.out.println("numx:" + numx);
        for (int i = ultimaPosicionCalculada + 1; i <= numx; i++) {
            System.out.println("Suma: " + a[i - 1] + " + " + calcularSumaDigitos(a[i - 1]));
            a[i] = a[i - 1] + calcularSumaDigitos(a[i - 1]);
        }
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int ultimaPosicionCalculada = 1;
        int[] a = new int[100];
        for (int i = 0; i < a.length; i++) a[i] = 0;
        a[0] = 1;
        a[1] = 1;

        while (true) {
            System.out.print("Posición: ");
            int numx = scan.nextInt();
            if (numx == 0) break;

            numx = numx - 1;

            if (ultimaPosicionCalculada < numx) {
                calcular(a, numx, ultimaPosicionCalculada);
                ultimaPosicionCalculada = numx;
            }

            System.out.println(Arrays.toString(a));
            System.out.println("Número calculado: " + a[numx] + " (en la posición " + numx + ")");
        }

        System.out.println("Fin.");
        scan.close();
    }
}
