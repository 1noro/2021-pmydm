package net.a3do.app.minesweeper.controller;

import android.widget.ImageView;

import net.a3do.app.minesweeper.R;

import java.util.Random;

// GameUtils, clase con métodos estáticos que se utilizan durante el desarrollo del juego
public class GameUtils {
    // bidimensionalArrayShuffle, desordena un array bidimensional que se le pasa como parámetro (copiado de stack overflow)
    public static void bidimensionalArrayShuffle(int[][] a) {
        Random random = new Random();
        for (int i = a.length - 1; i > 0; i--) {
            for (int j = a[i].length - 1; j > 0; j--) {
                int m = random.nextInt(i + 1);
                int n = random.nextInt(j + 1);

                int temp = a[i][j];
                a[i][j] = a[m][n];
                a[m][n] = temp;
            }
        }
    }

    // getImageViewX, obtenemos la posición X en el tablero de un image view en base a su ContentDescription
    public static int getImageViewX(ImageView imageView) {
        return Integer.parseInt(((String) imageView.getContentDescription()).split("x")[0]);
    }

    // getImageViewY, obtenemos la posición Y en el tablero de un image view en base a su ContentDescription
    public static int getImageViewY(ImageView imageView) {
        return Integer.parseInt(((String) imageView.getContentDescription()).split("x")[1]);
    }

    // getDrawableId, obtenemos el ID de la imagen dado una posicón (x, y) en el tablero
    public static int getDrawableId(GameLogic gameLogic, int x, int y) {
        int drawableId;
        switch (gameLogic.getNumbersArray()[y][x]) {
            default:
                drawableId = R.drawable.clicked_button;
                break;
            case 1:
                drawableId = R.drawable.clicked_button_1;
                break;
            case 2:
                drawableId = R.drawable.clicked_button_2;
                break;
            case 3:
                drawableId = R.drawable.clicked_button_3;
                break;
            case 4:
                drawableId = R.drawable.clicked_button_4;
                break;
            case 5:
                drawableId = R.drawable.clicked_button_5;
                break;
            case 6:
                drawableId = R.drawable.clicked_button_6;
                break;
            case 7:
                drawableId = R.drawable.clicked_button_7;
                break;
            case 8:
                drawableId = R.drawable.clicked_button_8;
                break;
        }
        return drawableId;
    }

}
