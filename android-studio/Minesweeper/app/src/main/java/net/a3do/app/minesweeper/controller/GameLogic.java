package net.a3do.app.minesweeper.controller;

import android.content.Context;
import android.os.Vibrator;
import android.util.Log;
import android.widget.ImageView;

import androidx.core.content.ContextCompat;

import net.a3do.app.minesweeper.R;
import net.a3do.app.minesweeper.controller.threads.MineExplosionThread;
import net.a3do.app.minesweeper.controller.threads.ZeroExpansionThread;
import net.a3do.app.minesweeper.view.GameActivity;
import net.a3do.app.minesweeper.view.MusicManager;

import java.util.Arrays;

// GaameLogic, calse instanciada desde GameActivity que se encarga de iterpretar las acciones del
// usuario y responder según el comportamiento del juego
public class GameLogic {
    private Context context;

    private int gridWidth;
    private int gridHeight;
    private int gridMines;

    private MusicManager musicManager;

    private int[][] numbersArray;
    private int[][] minesArray;
    private ImageView[][] imageViewArray;
    private int[][] destroyedArray;
    private int[][] clickedArray;
    private int[][] markedArray;

    private boolean levelFailed = false;
    private boolean levelFinished = false;

    public GameLogic(Context context, int gridWidth, int gridHeight, int gridMines, MusicManager musicManager) {
        this.context = context;
        this.gridWidth = gridWidth;
        this.gridHeight = gridHeight;
        this.gridMines = gridMines;
        this.musicManager = musicManager;

        // GENERAMOS TODOS LOS ARRAY S DE LA LOGICA Y LOS INICIALIZAMOS EN BASE A LA CONFIGURACIÓN DADA
        generateMinesArray();
        generateNumbersArray();
        this.imageViewArray = new ImageView[this.gridHeight][this.gridWidth];
        this.destroyedArray = new int[this.gridHeight][this.gridWidth];
        for (int[] row : this.destroyedArray) Arrays.fill(row,0);
        this.clickedArray = new int[this.gridHeight][this.gridWidth];
        for (int[] row : this.clickedArray) Arrays.fill(row,0);
        this.markedArray = new int[this.gridHeight][this.gridWidth];
        for (int[] row : this.markedArray) Arrays.fill(row,0);
    }

    // GETTERS Y SETTERS
    public int[][] getNumbersArray() {
        return numbersArray;
    }

    public int[][] getMinesArray() {
        return minesArray;
    }

    public ImageView[][] getImageViewArray() {
        return imageViewArray;
    }

    public int[][] getDestroyedArray() {
        return destroyedArray;
    }

    public void setDestroyedArrayPosition(int x, int y, int value) {
        this.destroyedArray[y][x] = value;
    }

    public int[][] getClickedArray() {
        return clickedArray;
    }

    public void setClickedArrayPosition(int x, int y, int value) {
        this.clickedArray[y][x] = value;
    }

    public int[][] getMarkedArray() {
        return markedArray;
    }

    public boolean isLevelFailed() {
        return levelFailed;
    }

    // printBiArray, imprime por l apantalla de LOG el array bidimensonal pasado como parametro
    public void printBiArray(int[][] biArray) {
        StringBuilder stringOut = new StringBuilder("biArray\n");
        for (int[] row : biArray)
            stringOut.append(Arrays.toString(row)).append("\n");
        Log.d("Info", stringOut.toString());
    }

    // generateMinesArray, genera el array de minas
    public void generateMinesArray() {
        this.minesArray = new int[this.gridHeight][this.gridWidth];

        for (int[] row : this.minesArray)
            Arrays.fill(row,0);
        int minesDeployed = 0;
        for (int y = 0; y < this.gridHeight; y++) {
            for (int x = 0; x < this.gridWidth; x++) {
                this.minesArray[y][x] = 1;
                minesDeployed++;
                if (minesDeployed >= this.gridMines) break;
            }
            if (minesDeployed >= this.gridMines) break;
        }

        GameUtils.bidimensionalArrayShuffle(this.minesArray);

        printBiArray(this.minesArray);
    }

    // generateNumbersArray, genera el array de números en base a l array de minas
    public void generateNumbersArray() {
        this.numbersArray = new int[this.gridHeight][this.gridWidth];
        for (int y = 0; y < gridHeight; y++) {
            for (int x = 0; x < gridWidth; x++) {
                if (this.minesArray[y][x] != 1) {
                    numberPlusMine(x, y, x - 1, y - 1);
                    numberPlusMine(x, y, x, y - 1);
                    numberPlusMine(x, y, x + 1, y - 1);
                    numberPlusMine(x, y, x - 1, y);
                    numberPlusMine(x, y, x + 1, y);
                    numberPlusMine(x, y, x - 1, y + 1);
                    numberPlusMine(x, y, x, y + 1);
                    numberPlusMine(x, y, x + 1, y + 1);
                }
            }
        }
    }

    // numberPlusMine, fragmento de código que sirbe para calcular los números del array de números
    public void numberPlusMine(int numberX, int numberY, int mineX, int mineY) {
        try {
            this.numbersArray[numberY][numberX] += this.minesArray[mineY][mineX];
        } catch (ArrayIndexOutOfBoundsException ignored) {}
    }

    // addImageView, sirve para que se pueda definir el array imageViewArray desde el GameActivity
    public void addImageView(int x, int y, ImageView imageView) {
        this.imageViewArray[y][x] = imageView;
    }

    // clickImageView, toda la lógica relaccionada con cada click realizado sobre una casilla del tablero
    public void clickImageView(ImageView imageView) {
        if (!this.levelFailed && !this.levelFinished) {
            int x = GameUtils.getImageViewX(imageView);
            int y = GameUtils.getImageViewY(imageView);
            if (this.markedArray[y][x] == 0 && this.clickedArray[y][x] == 0) {
                if (this.minesArray[y][x] == 1) {
                    this.levelFailed = true;
                    musicManager.stopBackgroundMusic();
                    musicManager.startEndGameEffect();
                    Vibrator v = (Vibrator) context.getSystemService(context.VIBRATOR_SERVICE);
                    v.vibrate(500);
                    (new MineExplosionThread(context, this, imageView)).start();
                } else {
                    if (this.numbersArray[y][x] == 0) {
                        (new ZeroExpansionThread(context, this, imageView)).start();
                    } else {
                        imageView.setImageDrawable(ContextCompat.getDrawable(this.context, GameUtils.getDrawableId(this, x, y)));
                        setClickedArrayPosition(x, y, 1);
                    }
                }
                if (checkLevelFinished() || this.levelFailed) {
                    this.levelFinished = true;
//                    Toast.makeText(context, "FINAL", Toast.LENGTH_SHORT).show();
                    ((GameActivity) context).finishUI();
                }
            }
        }
    }

    // markImageView, toda la lógica relaccionada con cada longClick realizado sobre una casilla del tablero (marcar = poner bandera)
    public void markImageView(ImageView imageView) {
        if (!this.levelFailed && !this.levelFinished) {
            int x = GameUtils.getImageViewX(imageView);
            int y = GameUtils.getImageViewY(imageView);
            if (this.clickedArray[y][x] == 0) {
                if (this.markedArray[y][x] != 1) {
                    imageView.setImageDrawable(ContextCompat.getDrawable(this.context, R.drawable.marked_button));
                    this.markedArray[y][x] = 1;
                    ((GameActivity) this.context).decrementMinesCounter();
                } else {
                    imageView.setImageDrawable(ContextCompat.getDrawable(this.context, R.drawable.default_button));
                    this.markedArray[y][x] = 0;
                    ((GameActivity) this.context).incrementMinesCounter();
                }
                if (checkLevelFinished()) {
                    this.levelFinished = true;
                    ((GameActivity) context).finishUI();
                }
            }
        }
    }

    // checkLevelFinished, comprobación de que el nivel está finalizado
    public boolean checkLevelFinished() {
        boolean levelFinished = false;
        int tilesClicked = 0;
        for (int y = 0; y < this.gridHeight; y++) {
            for (int x = 0; x < this.gridWidth; x++) {
                tilesClicked += this.clickedArray[y][x];
            }
        }
        int tilesNotClicked = (this.gridWidth * this.gridHeight) - tilesClicked;

        StringBuilder minesArrayStr = new StringBuilder();
        for (int[] row : this.minesArray) minesArrayStr.append(Arrays.toString(row));

        StringBuilder markedArrayStr = new StringBuilder();
        for (int[] row : this.markedArray) markedArrayStr.append(Arrays.toString(row));

        if (minesArrayStr.toString().equals(markedArrayStr.toString()) && tilesNotClicked == this.gridMines) {
            levelFinished = true;
        }
        return levelFinished;
    }

}
