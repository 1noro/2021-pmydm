package net.a3do.app.minesweeper.controller.threads;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.TextView;

// TimeCounterThread, hilo de ejecución que se encarga de actulizar el contador de segundos
// durante el juego
public class TimeCounterThread extends Thread {

    private Context context;
    private TextView timeCounter;
    private int seconds;

    public TimeCounterThread(Context context, TextView timeCounter) {
        this.context = context;
        this.timeCounter = timeCounter;
        this.seconds = 0;
    }

    @Override
    public void run() {
        try {
            while(true) {
                Thread.sleep(1000);
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        timeCounter.setText(String.valueOf(seconds));
                    }
                });
                seconds++;
            }
        } catch (InterruptedException ignored) {
            // Si el thread se interrumpe, se para de contar
            Log.d("TimeCounterThread", "Interrupted");
        }
    }
}
