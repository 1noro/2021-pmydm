package net.a3do.app.minesweeper.controller.threads;

import android.app.Activity;
import android.content.Context;
import android.widget.ImageView;

import androidx.core.content.ContextCompat;

import net.a3do.app.minesweeper.controller.GameLogic;
import net.a3do.app.minesweeper.controller.GameUtils;
import net.a3do.app.minesweeper.R;

// MineExplosionThread, hilo secundario que se encarga de la animación de las explosiones de las
// minas al parder la partida
public class MineExplosionThread extends Thread {

    private Context context;
    private GameLogic gameLogic;
    private ImageView imageView;
    private int x;
    private int y;

    public MineExplosionThread(Context context, GameLogic gameLogic, ImageView imageView) {
        this.context = context;
        this.gameLogic = gameLogic;
        this.imageView = imageView;
        this.x = GameUtils.getImageViewX(imageView);
        this.y = GameUtils.getImageViewY(imageView);
    }

    @Override
    public void run() {
        if (this.gameLogic.getDestroyedArray()[y][x] == 0) {
            ((Activity) this.context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int drawableId = R.drawable.destroyed_button;
//                    if (MineExplosionThread.this.isFirst) drawableId = R.drawable.clicked_button_m;
                    if (MineExplosionThread.this.gameLogic.getMinesArray()[y][x] == 1) drawableId = R.drawable.clicked_button_m;
                    imageView.setImageDrawable(ContextCompat.getDrawable(MineExplosionThread.this.context, drawableId));
                }
            });
            this.gameLogic.setDestroyedArrayPosition(x, y, 1);
            try {
                Thread.sleep((int) (Math.random() * 200) + 50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                (new MineExplosionThread(context, gameLogic, gameLogic.getImageViewArray()[y - 1][x - 1])).start();
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
            try {
                (new MineExplosionThread(context, gameLogic, gameLogic.getImageViewArray()[y - 1][x])).start();
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
            try {
                (new MineExplosionThread(context, gameLogic, gameLogic.getImageViewArray()[y - 1][x + 1])).start();
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
            try {
                (new MineExplosionThread(context, gameLogic, gameLogic.getImageViewArray()[y][x - 1])).start();
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
            try {
                (new MineExplosionThread(context, gameLogic, gameLogic.getImageViewArray()[y][x + 1])).start();
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
            try {
                (new MineExplosionThread(context, gameLogic, gameLogic.getImageViewArray()[y + 1][x - 1])).start();
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
            try {
                (new MineExplosionThread(context, gameLogic, gameLogic.getImageViewArray()[y + 1][x])).start();
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
            try {
                (new MineExplosionThread(context, gameLogic, gameLogic.getImageViewArray()[y + 1][x + 1])).start();
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
        }
    }
}
