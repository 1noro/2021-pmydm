package net.a3do.app.minesweeper.view;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import net.a3do.app.minesweeper.R;

// MainActivity calase inicial del juego, y menú de selección de niveles
public class MainActivity extends AppCompatActivity {

    public static final String LEVEL_CONFIG = "net.a3do.app.minesweeper.LEVEL_CONFIG";
    public static final String MUSIC_MANAGER = "net.a3do.app.minesweeper.MUSIC_MANAGER";
    public static int buttonTextSize = 24;
    public static int buttonTextSizeReduction = 1;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_fancy);

        // ahora asignamos los listeners a los botones del menú
        // BEGINNER
        ConstraintLayout buttonLevel = findViewById(R.id.buttonLevel0);
        buttonLevel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                ImageView buttonImage = (ImageView) ((ViewGroup) view).getChildAt(0);
                TextView buttonText = (TextView) ((ViewGroup) view).getChildAt(1);
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // touch down code
                        buttonImage.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.clicked_menu_button));
                        buttonText.setTextSize(TypedValue.COMPLEX_UNIT_SP, buttonTextSize - buttonTextSizeReduction);
                        break;
                    case MotionEvent.ACTION_UP:
                        // touch up code
                        buttonImage.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.menu_button));
                        buttonText.setTextSize(TypedValue.COMPLEX_UNIT_SP, buttonTextSize);
                        startLevel(new int[]{8, 8, 10});
                        break;
                }
                return true;
            }
        });

        // INTERMEDIATE
        buttonLevel = findViewById(R.id.buttonLevel1);
        buttonLevel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                ImageView buttonImage = (ImageView) ((ViewGroup) view).getChildAt(0);
                TextView buttonText = (TextView) ((ViewGroup) view).getChildAt(1);
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // touch down code
                        buttonImage.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.clicked_menu_button));
                        buttonText.setTextSize(TypedValue.COMPLEX_UNIT_SP, buttonTextSize - buttonTextSizeReduction);
                        break;
                    case MotionEvent.ACTION_UP:
                        // touch up code
                        buttonImage.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.menu_button));
                        buttonText.setTextSize(TypedValue.COMPLEX_UNIT_SP, buttonTextSize);
                        startLevel(new int[]{16, 16, 40});
                        break;
                }
                return true;
            }
        });

        // EXPERT
        buttonLevel = findViewById(R.id.buttonLevel2);
        buttonLevel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                ImageView buttonImage = (ImageView) ((ViewGroup) view).getChildAt(0);
                TextView buttonText = (TextView) ((ViewGroup) view).getChildAt(1);
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // touch down code
                        buttonImage.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.clicked_menu_button));
                        buttonText.setTextSize(TypedValue.COMPLEX_UNIT_SP, buttonTextSize - buttonTextSizeReduction);
                        break;
                    case MotionEvent.ACTION_UP:
                        // touch up code
                        buttonImage.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.menu_button));
                        buttonText.setTextSize(TypedValue.COMPLEX_UNIT_SP, buttonTextSize);
                        startLevel(new int[]{16, 30, 99});
                        break;
                }
                return true;
            }
        });

        // CUSTOM
        buttonLevel = findViewById(R.id.buttonLevelCustom);
        buttonLevel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                ImageView buttonImage = (ImageView) ((ViewGroup) view).getChildAt(0);
                TextView buttonText = (TextView) ((ViewGroup) view).getChildAt(1);
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // touch down code
                        buttonImage.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.clicked_menu_button));
                        buttonText.setTextSize(TypedValue.COMPLEX_UNIT_SP, buttonTextSize - buttonTextSizeReduction);
                        break;
                    case MotionEvent.ACTION_UP:
                        // touch up code
                        buttonImage.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.menu_button));
                        buttonText.setTextSize(TypedValue.COMPLEX_UNIT_SP, buttonTextSize);
                        Intent intent = new Intent(getApplicationContext(), CustomDialogActivity.class);
                        startActivity(intent);
                        break;
                }
                return true;
            }
        });

    }

    // startLevel, función que inicia una nueva GameActivity con una configuración determinada
    public void startLevel(int[] levelConfig) {
        Intent intent = new Intent(getApplicationContext(), GameActivity.class);
        intent.putExtra(LEVEL_CONFIG, levelConfig);
        startActivity(intent);
    }
}