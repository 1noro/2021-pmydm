package net.a3do.app.minesweeper.view;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import net.a3do.app.minesweeper.R;
import net.a3do.app.minesweeper.controller.GameLogic;
import net.a3do.app.minesweeper.controller.threads.TimeCounterThread;

import java.util.Objects;

import static net.a3do.app.minesweeper.view.MainActivity.LEVEL_CONFIG;

// GameActivity, clase configurable según el nivel done se desarrolla el juego
public class GameActivity extends AppCompatActivity {

    private int gridWidth;
    private int gridHeight;
    private int gridMines;

    private GameLogic gameLogic;
    public static MusicManager musicManager;

    private TextView minesCounter;
    private ImageView controlButton;
    private TextView timeCounter;

    private TimeCounterThread timeCounterThread = null;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        musicManager = new MusicManager(getApplicationContext());

        // Quitamos la barra del titulo
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(getSupportActionBar()).hide();

        // Obtenemos los datos de la configuración del nivel
        Intent intent = getIntent();
        int[] levelConfig = intent.getIntArrayExtra(LEVEL_CONFIG);
        try {
            assert levelConfig != null;
            gridWidth = levelConfig[0];
            gridHeight = levelConfig[1];
            gridMines = levelConfig[2];
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }

        // Mostramos el XML
        setContentView(R.layout.activity_game_scroll);

        // Obtenemos las Views del XML
        this.minesCounter = findViewById(R.id.minesCounter);
        this.controlButton = findViewById(R.id.controlButton);
        this.timeCounter = findViewById(R.id.timeCounter);

        this.minesCounter.setText(String.valueOf(gridMines));
        this.minesCounter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(GameActivity.this, getResources().getString(R.string.mines_counter_description), Toast.LENGTH_SHORT).show();
            }
        });

        this.controlButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // touch down code
                        ((ImageView) view).setImageDrawable(ContextCompat.getDrawable(GameActivity.this, R.drawable.clicked_control_button));
                        break;
                    case MotionEvent.ACTION_UP:
                        // touch up code
                        ((ImageView) view).setImageDrawable(ContextCompat.getDrawable(GameActivity.this, R.drawable.default_control_button));
                        startNewLevel();
                        break;
                }
                return true;
            }
        });

        this.timeCounter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(GameActivity.this, getResources().getString(R.string.time_counter_description), Toast.LENGTH_SHORT).show();
            }
        });

        // iniciamos un nuevo nivel con l aconfiguración obtenida
        startNewLevel();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.timeCounterThread.interrupt();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (musicManager.isBackgroundMusicPlaying()) musicManager.pauseBackgroundMusic();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!musicManager.isBackgroundMusicPlaying()) musicManager.continueBackgroundMusic();
    }

    // startNewLevel, método al que llamar para iniciar un nuevo nivel en base a una configuración dada
    public void startNewLevel() {
        musicManager.startBackgroundMusic();
        this.gameLogic = new GameLogic(this, gridWidth, gridHeight, gridMines, musicManager);
        if (this.timeCounterThread != null) this.timeCounterThread.interrupt();
        this.timeCounterThread = new TimeCounterThread(this, timeCounter);
        this.minesCounter.setText(String.valueOf(this.gridMines));
        this.timeCounter.setText(getResources().getString(R.string.time_counter_sample_text));
        generateButtonGrid(gridWidth, gridHeight);
        this.timeCounterThread.start();
    }

    // generateButtonGrid, genera, calcula las posiciones y muestra el tablero del nivel
    public void generateButtonGrid(int gridWidth, int gridHeight) {
        int screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
        int buttonSize = screenWidth / gridWidth;

        LinearLayout linearLayout = findViewById(R.id.gridLayout);
        linearLayout.removeAllViews();

        for (int y = 0; y < gridHeight; y++) {
            LinearLayout linearLayoutRow = new LinearLayout(this);
            LinearLayout.LayoutParams linearLayoutRowLp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            linearLayoutRow.setLayoutParams(linearLayoutRowLp);
            linearLayoutRow.setOrientation(LinearLayout.HORIZONTAL);
            for (int x = 0; x < gridWidth; x++) {
                ImageView imageView = new ImageView(this);
                LinearLayout.LayoutParams imageViewLp = new LinearLayout.LayoutParams(buttonSize, buttonSize);
                imageView.setLayoutParams(imageViewLp);
                imageView.setContentDescription(x + "x" + y);
                imageView.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.default_button));
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        GameActivity.this.gameLogic.clickImageView((ImageView) view);
                    }
                });
                imageView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        GameActivity.this.gameLogic.markImageView((ImageView) view);
                        return true;
                    }
                });
                linearLayoutRow.addView(imageView);
                this.gameLogic.addImageView(x, y, imageView);
            }
            linearLayout.addView(linearLayoutRow);
        }
    }

    // finishUI, cuando acabamos el nivel paramos tódo lo que se está moviendo en la interfaz gráfica
    public void finishUI() {
        this.timeCounterThread.interrupt();
        int drawableId = R.drawable.success_control_button;
        if (gameLogic.isLevelFailed()) drawableId = R.drawable.failed_control_button;
        this.controlButton.setImageDrawable(ContextCompat.getDrawable(GameActivity.this, drawableId));
    }

    // incrementMinesCounter, suma 1 al contador de minas restantes
    public void incrementMinesCounter() {
        int currentCount = Integer.parseInt((String) this.minesCounter.getText());
        this.minesCounter.setText(String.valueOf(currentCount + 1));
    }

    // decrementMinesCounter, resta 1 al contador de minas restantes
    public void decrementMinesCounter() {
        int currentCount = Integer.parseInt((String) this.minesCounter.getText());
        this.minesCounter.setText(String.valueOf(currentCount - 1));
    }

}
