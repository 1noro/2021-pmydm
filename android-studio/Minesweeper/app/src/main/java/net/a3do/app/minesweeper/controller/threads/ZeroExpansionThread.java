package net.a3do.app.minesweeper.controller.threads;

import android.app.Activity;
import android.content.Context;
import android.widget.ImageView;

import androidx.core.content.ContextCompat;

import net.a3do.app.minesweeper.controller.GameLogic;
import net.a3do.app.minesweeper.controller.GameUtils;

// ZeroExpansionThread, hilo secundario que se encarga de la animación al pulsar sobre una casilla
// de ceros (vacía)
public class ZeroExpansionThread extends Thread {

    private Context context;
    private GameLogic gameLogic;
    private ImageView imageView;
    private int x;
    private int y;

    public ZeroExpansionThread(Context context, GameLogic gameLogic, ImageView imageView) {
        this.context = context;
        this.gameLogic = gameLogic;
        this.imageView = imageView;
        this.x = GameUtils.getImageViewX(imageView);
        this.y = GameUtils.getImageViewY(imageView);
    }

    @Override
    public void run() {
        if (this.gameLogic.getClickedArray()[y][x] == 0 && this.gameLogic.getMarkedArray()[y][x] == 0) {
            ((Activity) this.context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    imageView.setImageDrawable(ContextCompat.getDrawable(context, GameUtils.getDrawableId(gameLogic, x, y)));
                }
            });
            this.gameLogic.setClickedArrayPosition(x, y, 1);
            if (this.gameLogic.getNumbersArray()[y][x] == 0) {
                try {
                    Thread.sleep((int) (Math.random() * 200) + 50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    (new ZeroExpansionThread(context, gameLogic, gameLogic.getImageViewArray()[y - 1][x - 1])).start();
                } catch (ArrayIndexOutOfBoundsException ignored) {
                }
                try {
                    (new ZeroExpansionThread(context, gameLogic, gameLogic.getImageViewArray()[y - 1][x])).start();
                } catch (ArrayIndexOutOfBoundsException ignored) {
                }
                try {
                    (new ZeroExpansionThread(context, gameLogic, gameLogic.getImageViewArray()[y - 1][x + 1])).start();
                } catch (ArrayIndexOutOfBoundsException ignored) {
                }
                try {
                    (new ZeroExpansionThread(context, gameLogic, gameLogic.getImageViewArray()[y][x - 1])).start();
                } catch (ArrayIndexOutOfBoundsException ignored) {
                }
                try {
                    (new ZeroExpansionThread(context, gameLogic, gameLogic.getImageViewArray()[y][x + 1])).start();
                } catch (ArrayIndexOutOfBoundsException ignored) {
                }
                try {
                    (new ZeroExpansionThread(context, gameLogic, gameLogic.getImageViewArray()[y + 1][x - 1])).start();
                } catch (ArrayIndexOutOfBoundsException ignored) {
                }
                try {
                    (new ZeroExpansionThread(context, gameLogic, gameLogic.getImageViewArray()[y + 1][x])).start();
                } catch (ArrayIndexOutOfBoundsException ignored) {
                }
                try {
                    (new ZeroExpansionThread(context, gameLogic, gameLogic.getImageViewArray()[y + 1][x + 1])).start();
                } catch (ArrayIndexOutOfBoundsException ignored) {
                }
            }
        }
    }
}
