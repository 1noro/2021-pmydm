package net.a3do.app.minesweeper.view;

import android.content.Context;
import android.media.MediaPlayer;

import net.a3do.app.minesweeper.R;

// MusicManager, encargada de gestionar y organizar los efectos de sonido que se reproducen durante el juego
public class MusicManager {
    private Context context;
    private final int backgroundMusic = R.raw.aphex_twin_xtal;
    private final int endGameEffect = R.raw.game_end;
    private MediaPlayer backgroundMediaPlayer = null;

    public MusicManager(Context context) {
        this.context = context;
//        this.backgroundMediaPlayer = MediaPlayer.create(context, backgroundMusic);
//        this.backgroundMediaPlayer.setLooping(true);
    }

    public void startBackgroundMusic() {
        if (backgroundMediaPlayer != null && backgroundMediaPlayer.isPlaying()) backgroundMediaPlayer.stop();
        backgroundMediaPlayer = MediaPlayer.create(context, backgroundMusic);
        backgroundMediaPlayer.setLooping(true);
        backgroundMediaPlayer.start();
    }

    public void continueBackgroundMusic() {
        backgroundMediaPlayer.start();
    }

    public void pauseBackgroundMusic() {
        backgroundMediaPlayer.pause();
    }

    public void stopBackgroundMusic() {
        backgroundMediaPlayer.stop();
    }

    public boolean isBackgroundMusicPlaying() {
        return backgroundMediaPlayer.isPlaying();
    }

    public void startEndGameEffect() {

        MediaPlayer effectMediaPlayer = MediaPlayer.create(context, endGameEffect);
        effectMediaPlayer.start();
    }

}
