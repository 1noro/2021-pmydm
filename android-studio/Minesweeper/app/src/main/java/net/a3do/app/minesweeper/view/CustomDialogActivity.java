package net.a3do.app.minesweeper.view;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import net.a3do.app.minesweeper.R;

import static net.a3do.app.minesweeper.view.MainActivity.LEVEL_CONFIG;

// CustomDialogActivity, clase que se abre en formato Dialog para seleccionar la dificultad del nivel CUSTOM
public class CustomDialogActivity extends AppCompatActivity {

    private TextView resultText;
    private SeekBar minesBar;
    private int customWidth = 8;
    private int customHeight = 8;
    private int customMines = 10;

    public static int buttonTextSize = 24;
    public static int buttonTextSizeReduction = 1;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Mostramos el XML
        setContentView(R.layout.activity_custom_dialog);

        // obtenemos los elementos del XML y les asignamos sos correspondientes listeners
        this.resultText = findViewById(R.id.resultText);
        this.minesBar = findViewById(R.id.minesBar);

        SeekBar widthBar = findViewById(R.id.widthBar);
        widthBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                customWidth = progress + 8;
                String text = customWidth + " x " + customHeight + " (" + customMines + ")";
                int maxMines = (customWidth * customHeight) - 2;
                if (maxMines < 170)
                    minesBar.setMax(maxMines);
                else
                    minesBar.setMax(168);
                resultText.setText(text);
            }
        });

        SeekBar heightBar = findViewById(R.id.heightBar);
        heightBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                customHeight = progress + 8;
                String text = customWidth + " x " + customHeight + " (" + customMines + ")";
                int maxMines = (customWidth * customHeight) - 2;
                if (maxMines < 170)
                    minesBar.setMax(maxMines);
                else
                    minesBar.setMax(168);
                resultText.setText(text);
            }
        });

        minesBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                customMines = progress + 2;
                String text = customWidth + " x " + customHeight + " (" + customMines + ")";
                resultText.setText(text);
            }
        });

//        Button playButton = findViewById(R.id.playButtonOld);
//        playButton.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View button) {
//                Intent intent = new Intent(getApplicationContext(), GameActivity.class);
//                int[] levelConfig = {customWidth, customHeight, customMines};
//                intent.putExtra(EXTRA_MESSAGE, levelConfig);
//                startActivity(intent);
//                finish(); // para que al volver no siga desplegado el dialog
//            }
//        });

        ConstraintLayout playButton = findViewById(R.id.playButton);
        playButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                ImageView buttonImage = (ImageView) ((ViewGroup) view).getChildAt(0);
                TextView buttonText = (TextView) ((ViewGroup) view).getChildAt(1);
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // touch down code
                        buttonImage.setImageDrawable(ContextCompat.getDrawable(CustomDialogActivity.this, R.drawable.clicked_menu_button));
                        buttonText.setTextSize(TypedValue.COMPLEX_UNIT_SP, buttonTextSize - buttonTextSizeReduction);
                        break;
                    case MotionEvent.ACTION_UP:
                        // touch up code
                        buttonImage.setImageDrawable(ContextCompat.getDrawable(CustomDialogActivity.this, R.drawable.menu_button));
                        buttonText.setTextSize(TypedValue.COMPLEX_UNIT_SP, buttonTextSize);

                        Intent intent = new Intent(getApplicationContext(), GameActivity.class);
                        int[] levelConfig = {customWidth, customHeight, customMines};
                        intent.putExtra(LEVEL_CONFIG, levelConfig);
                        startActivity(intent);
                        finish(); // para que al volver no siga desplegado el dialog

                        break;
                }
                return true;
            }
        });

    }

}
