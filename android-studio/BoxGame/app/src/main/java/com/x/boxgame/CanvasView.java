package com.x.boxgame;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

import java.util.Random;

public class CanvasView extends SurfaceView implements SurfaceHolder.Callback {

    private Context context;

    private volatile boolean isPlaying = true;

    //-- TAMAÑOS ------------------------------------
    //Tamaño de la pantalla en píxeles
    private int screenX;
    private int screenY;

    //Tamaño de cada pixel de la serpiente
    private int blockSize;

    //Tamaño en bloques del area jugable
    private final int numBlocksWide = 40;
    private int numBlocksHigh;

    //Localización en la malla de todos los bloques
    private int[] snakeXs;
    private int[] snakeYs;

    //Localización de bob
    private int bobX;
    private int bobY;

    //Longitud de la serpiente
    private int snakeLength;

    //Dirección de la serpiente
    private char dir;

    //-- REFRESH -----------------------------------
    // Control pausing between updates
    private long nextFrameTime;

    private long FPS = 0;
    private final long MILLIS_PER_SECOND = 1000;

    //-- PINTAR ------------------------------------
    private Canvas canvas;
    private Paint paint;

    //-- GET THREAD ------------------------------------
    private MainLoop mainLoop;

    public CanvasView(Context context, AttributeSet attrs) {
        super(context, attrs);

        getHolder().addCallback(this);
        mainLoop = new MainLoop(getHolder(),this);

        paint = new Paint();
        this.context = context;

        snakeXs = new int[100];
        snakeYs = new int[100];

        newGame();
    }

    public MainLoop getMainLoop() {
        return mainLoop;
    }

    public void getFPS(int fps){
        this.FPS = fps;
    }

    public void newGame(){
        snakeLength = 1;
        //Posicionar a la serpiente en el medio
        snakeXs[0] = numBlocksWide / 2;
        snakeYs[0] = 50 / 2;

        bobX = 10;
        bobY = 10;

        nextFrameTime = System.currentTimeMillis();
    }

    public void spawnBob() {
        Random random = new Random();
        bobX = random.nextInt(numBlocksWide - 1) + 1;
        bobY = random.nextInt(numBlocksHigh - 1) + 1;
    }

    public void moveSnake(char dir){
        this.dir=dir;

        for (int i = snakeLength; i > 0; i--) {
            snakeXs[i] = snakeXs[i - 1];
            snakeYs[i] = snakeYs[i - 1];
        }

        switch (dir) {
            case 'u':
                Log.d("button","pulsado boton arriba");
                snakeYs[0]--;
                break;
            case 'r':
                Log.d("button","pulsado boton derecha");
                snakeXs[0]++;
                break;
            case 'l':
                Log.d("button","pulsado boton izquierda");
                snakeXs[0]--;
                break;
            case 'd':
                Log.d("button","pulsado boton abajo");
                snakeYs[0]++;
                break;
        }
    }

    private boolean detectDeath(){
        boolean dead = false;

        //SI CHOCA CON LA PANTALLA
        if (snakeXs[0] == -1) dead = true;
        if (snakeXs[0] >= numBlocksWide) dead = true;
        if (snakeYs[0] == -1) dead = true;
        if (snakeYs[0] == numBlocksHigh) dead = true;

        //SI SE COME A SI MISMO
        for (int i = snakeLength - 1; i > 0; i--) {
            if ((i > 4) && (snakeXs[0] == snakeXs[i]) && (snakeYs[0] == snakeYs[i])) {
                dead = true;
                break;
            }
        }

        return dead;
    }

    public boolean updateTime() {

        if(nextFrameTime <= System.currentTimeMillis()){
            nextFrameTime = System.currentTimeMillis() + MILLIS_PER_SECOND / FPS;
            return true;
        }
        return false;
    }

    public void update() {

        //COMERSE A BOB
        if (snakeXs[0] == bobX && snakeYs[0] == bobY) {
            snakeLength++;
            spawnBob();
        }

        if (updateTime()) {
            invalidate();
            moveSnake(dir);
        }

        if (detectDeath())
            newGame();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawRGB(0,0,0);

        screenX = getWidth();
        screenY = getHeight();
        
        blockSize = screenX / numBlocksWide;
        numBlocksHigh = screenY / blockSize;

        paint.setColor(Color.rgb( 255, 255, 255));

        //Para dibujar la serpiente un bloque cada vez
        for (int i = 0; i < snakeLength; i++) {
            canvas.drawRect(snakeXs[i] * blockSize,
                    (snakeYs[i] * blockSize),
                    (snakeXs[i] * blockSize) + blockSize,
                    (snakeYs[i] * blockSize) + blockSize,
                    paint);
        }

        paint.setColor(Color.argb(255, 255, 0, 0));

        // Draw Bob
        canvas.drawRect(bobX * blockSize, (bobY * blockSize), (bobX * blockSize) + blockSize, (bobY * blockSize) + blockSize, paint);
    }

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder surfaceHolder) {
        setWillNotDraw(false);

        canvas = getHolder().lockCanvas();
        draw(canvas);
        getHolder().unlockCanvasAndPost(canvas);

        this.mainLoop.setPlaying(this.isPlaying);
        this.mainLoop.start();
    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder surfaceHolder) {

    }
}
