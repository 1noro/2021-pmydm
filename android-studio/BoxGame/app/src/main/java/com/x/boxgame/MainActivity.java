package com.x.boxgame;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Configuración del botón fácil
        ImageView easyButton = findViewById(R.id.easyButton);
        iniciarApp(easyButton,5);

        //Configuración del botón medio
        ImageView mediumButton = findViewById(R.id.mediumButton);
        iniciarApp(mediumButton,10);

        //Configuración del botón dificil
        ImageView hardButton = findViewById(R.id.hardButton);
        iniciarApp(hardButton,20);
    }

    public void iniciarApp(ImageView button, final int speed) {
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                // Intentar abrir la segunda pantalla mandando la configuración
                Intent intent = new Intent(getApplicationContext(), GameActivity.class);
                int[] levelConfig = {speed};
                intent.putExtra("datos", levelConfig);
                startActivity(intent);
            }
        });
    }
}