package com.x.boxgame;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class MainLoop extends Thread {

    private boolean isPlaying;
    private final SurfaceHolder surfaceHolder;
    private final CanvasView canvasView;

    public MainLoop(SurfaceHolder surfaceHolder, CanvasView canvasView) {
        super();
        this.surfaceHolder = surfaceHolder;
        this.canvasView = canvasView;
    }

    public void setPlaying(boolean playing) {
        this.isPlaying = playing;
    }

    @Override
    public void run() {
        Canvas canvas = null;

        //MAIN LOOP
        while (isPlaying) {
            try {
                //Obtener el canvas de la vista
                canvas = this.surfaceHolder.lockCanvas();
                synchronized (surfaceHolder) {
                    this.canvasView.update();
                }

            } finally {
                if (canvas != null)
                    surfaceHolder.unlockCanvasAndPost(canvas);
            }
        }
    }
}
