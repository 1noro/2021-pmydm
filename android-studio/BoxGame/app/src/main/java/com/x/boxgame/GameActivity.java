package com.x.boxgame;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class GameActivity extends AppCompatActivity {

    private CanvasView canvasView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();

        int[] gameConfig = intent.getIntArrayExtra("datos");
        int speed = gameConfig[0];

        setContentView(R.layout.activity_game);

        canvasView = findViewById(R.id.canvasView);

        canvasView.getFPS(speed);

        final Button upButton = findViewById(R.id.upButton);
        upButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                canvasView.moveSnake('u');
            }
        });

        final Button rightButton = findViewById(R.id.rightButton);
        rightButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                canvasView.moveSnake('r');
            }
        });

        final Button leftButton = findViewById(R.id.leftButton);
        leftButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                canvasView.moveSnake('l');
            }
        });

        final Button downButton = findViewById(R.id.downButton);
        downButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                canvasView.moveSnake('d');
            }
        });

    }

    @Override
    protected void onDestroy() {
        //Para controlar posible error de parada
        try {
            this.canvasView.getMainLoop().setPlaying(false);
        } catch (IllegalArgumentException ignored) {}

        super.onDestroy();
    }
}
