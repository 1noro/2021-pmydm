package net.a3do.app.findyourself.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import net.a3do.app.findyourself.model.Level;

public class MyCanvas extends View {

    private Level level;

    private int width;
    private int height;

    private int levelDx = 0;
    private int levelDy = 0;

//    private final float TOUCH_SCALE_FACTOR = 180.0f / 320;
    private float previousX;
    private float previousY;

    public MyCanvas(Context context, Level level) {
        super(context);
        this.level = level;
    }

    @Override
    protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
        this.width = width;
        this.height = height;

        this.level.generate(width, height);

        super.onSizeChanged(width, height, oldWidth, oldHeight);
        Log.d("MyCanvas width", "" + this.width);
        Log.d("MyCanvas height", "" + this.height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRGB(0,0,0);
        level.draw(canvas, this.levelDx, this.levelDy);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        // MotionEvent reports input details from the touch screen
        // and other input controls. In this case, you are only
        // interested in events where the touch position changed.

        float x = e.getX();
        float y = e.getY();

        switch (e.getAction()) {
            case MotionEvent.ACTION_MOVE:

                float dxFloat = x - previousX;
                float dyFloat = y - previousY;

                // reverse direction of rotation above the mid-line
//                if (y > getHeight() / 2) {
//                    dx = dx * -1 ;
//                }

                // reverse direction of rotation to left of the mid-line
//                if (x < getWidth() / 2) {
//                    dy = dy * -1 ;
//                }

                int dx = (int) dxFloat;
                int dy = (int) dyFloat;

                int xReducer = dx < 0 ? -1 : 1;
                int yReducer = dy < 0 ? -1 : 1;

                while (this.level.isThereACollision(dx, dy)) {
                    Log.d("move calc", "(" + dx + ", " + dy + "): " + this.level.isThereACollision(dx, dy));
                    dx = dx + xReducer;
                    dy = dy + yReducer;
                }

                this.levelDx = dx;
                this.levelDy = dy;

                Log.d("move", "(" + dx + ", " + dy + "): " + this.level.isThereACollision(dx, dy));

//                if (!this.level.isThereACollision(dx, dy)) {
                    invalidate(); // llama al onDraw()
//                }

//                renderer.setAngle(renderer.getAngle() + ((dx + dy) * TOUCH_SCALE_FACTOR));
//                requestRender();
        }

        previousX = x;
        previousY = y;
        return true;
    }

}
