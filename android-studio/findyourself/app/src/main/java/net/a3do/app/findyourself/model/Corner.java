package net.a3do.app.findyourself.model;

public class Corner {

    private int x;
    private int y;

    public Corner(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isLeftOf(Corner corner) {
        return this.x <= corner.getX();
    }

    public boolean isRightOf(Corner corner) {
        return this.x >= corner.getX();
    }

    // encima
    public boolean isAboveOf(Corner corner) {
        return this.y <= corner.getY();
    }

    // debajo
    public boolean isBelowOf(Corner corner) {
        return this.y >= corner.getY();
    }

    public boolean isBetweenX(Corner cornerLeft, Corner cornerRight) {
        return this.x >= cornerLeft.getX() && this.x <= cornerRight.getX();
    }

    public boolean isBetweenY(Corner cornerTop, Corner cornerBottom) {
        return this.y >= cornerTop.getY() && this.y <= cornerBottom.getY();
    }
}
