package net.a3do.app.findyourself.model;

import android.graphics.Canvas;
import android.util.Log;

public class Level {

    private int sideSize;

    private int[][] map;
    private Wall[][] wallMap;
    private Player player;
    private Goal goal;

//    private int canvasWidth;
//    private int canvasHeight;
    private int playerX;
    private int playerY;

    private boolean generated = false;

    public Level(int[][] map, int sideSize) {
        this.map = map;
        this.sideSize = sideSize;
    }

    public void generate(int canvasWidth, int canvasHeight) {
        if (!this.generated) {
//            this.canvasWidth = canvasWidth;
//            this.canvasHeight = canvasHeight;

            int centerX = canvasWidth / 2;
            int centerY = canvasHeight / 2;

            this.playerX = centerX - this.sideSize / 2;
            this.playerY = centerY - this.sideSize / 2;

            int firstBlockX = 0;
            int firstBlockY = 0;
            for (int row = 0; row < this.map.length; row++) {
                for (int col = 0; col < this.map[row].length; col++) {
                    if (this.map[row][col] == 2) {
                        firstBlockX = (centerX - this.sideSize / 2) - col * this.sideSize;
                        firstBlockY = (centerY - this.sideSize / 2) - row * this.sideSize;
                    }
                }
            }

            try {
                StringBuilder mapStr = new StringBuilder();
                this.wallMap = new Wall[map.length][map[0].length];
                for (int row = 0; row < this.map.length; row++) {
                    for (int col = 0; col < this.map[row].length; col++) {
                        int x = firstBlockX + col * this.sideSize;
                        int y = firstBlockY + row * this.sideSize;
                        switch (this.map[row][col]) {
                            case 0:
                                mapStr.append(" ");
                                this.wallMap[row][col] = null;
                                break;
                            case 1:
                                mapStr.append("#");
                                this.wallMap[row][col] = new Wall(x, y, this.sideSize);
                                break;
                            case 2:
                                mapStr.append("@");
                                this.wallMap[row][col] = null;
                                this.player = new Player(x, y, this.sideSize);
                                break;
                            case 3:
                                mapStr.append("$");
                                this.wallMap[row][col] = null;
                                this.goal = new Goal(x, y, this.sideSize);
                                break;
                        }
                    }
                    mapStr.append("\n");
                }
                Log.d("Map", "a\n" + mapStr.toString());
            } catch (NullPointerException e) {
                System.out.println("Error: Posiblemente el map no tenga ninguna row.");
                e.printStackTrace();
            }

            this.generated = true;
        }
    }

    public boolean isThereACollision(int dx, int dy) {
        boolean colliding = false;
        for (Wall[] row : this.wallMap) {
            for (Wall wall : row) {
                if (wall != null) {
                    if (wall.isColliding(dx, dy, this.playerX, this.playerY)) {
                        colliding = true;
                    }
                }
            }
        }
        return colliding;
    }

    public void draw(Canvas canvas, int dx, int dy) {
        if (this.generated) {
            for (Wall[] row : this.wallMap) {
                for (Wall wall : row) {
                    if (wall != null) {
                        wall.draw(canvas, dx, dy);
                    }
                }
            }

            this.goal.draw(canvas, dx, dy);
            this.player.draw(canvas, 0, 0);
        }
    }

}
