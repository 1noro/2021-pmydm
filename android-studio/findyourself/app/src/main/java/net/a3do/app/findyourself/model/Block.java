package net.a3do.app.findyourself.model;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

public class Block {

    private int sideSize;

    private int x;
    private int y;

    private Paint paint;

    public Block(int x, int y, int sideSize) {
        this.sideSize = sideSize;

        this.x = x;
        this.y = y;

        this.paint = new Paint();
        this.paint.setColor(Color.BLUE);
    }

    public void draw(Canvas canvas, int dx, int dy) {
        this.x += dx;
        this.y += dy;
        Rect rect = new Rect(this.x, this.y ,this.x + this.sideSize,this.y + this.sideSize);
        canvas.drawRect(rect, this.paint);
    }

    public void setColor(int color) {
        this.paint.setColor(color);
    }

    public boolean isColliding(int dx, int dy, int playerX, int playerY) {
        boolean colliding = false;

        // calculando nuevas esquinas del Block
        Corner bTopLeft = new Corner(this.x + dx, this.y + dy);
        Corner bTopRight = new Corner((bTopLeft.getX() + this.sideSize) + dx, bTopLeft.getY() + dy);
        Corner bBottomLeft = new Corner(bTopLeft.getX() + dx, (bTopLeft.getY()+ this.sideSize) + dy);
        Corner bBottomRight = new Corner((bTopLeft.getX() + this.sideSize) + dx, (bTopLeft.getY() + this.sideSize) + dy);

        // calculando esquinas del Player
        Corner pTopLeft = new Corner(playerX, playerY);
        Corner pTopRight = new Corner(pTopLeft.getX() + this.sideSize, pTopLeft.getY());
        Corner pBottomLeft = new Corner(pTopLeft.getX(), pTopLeft.getY() + this.sideSize);
//        Corner pBottomRight = new Corner(pTopLeft.getX() + this.sideSize, pTopLeft.getY() + this.sideSize);

        // ahora a comprobar si las esquinas se superponen
        if (
                bTopLeft.isBetweenX(pTopLeft, pTopRight) && bTopLeft.isBetweenY(pTopLeft, pBottomLeft) ||
                bTopRight.isBetweenX(pTopLeft, pTopRight) && bTopRight.isBetweenY(pTopLeft, pBottomLeft) ||
                bBottomLeft.isBetweenX(pTopLeft, pTopRight) && bBottomLeft.isBetweenY(pTopLeft, pBottomLeft) ||
                bBottomRight.isBetweenX(pTopLeft, pTopRight) && bBottomRight.isBetweenY(pTopLeft, pBottomLeft)
        ) {
            colliding = true;
        }

        return colliding;
    }
}
