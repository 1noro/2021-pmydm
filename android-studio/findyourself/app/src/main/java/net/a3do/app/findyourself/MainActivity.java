package net.a3do.app.findyourself;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import net.a3do.app.findyourself.model.Level;
import net.a3do.app.findyourself.view.MyCanvas;

public class MainActivity extends AppCompatActivity {

    private int sideSize = 50;
    private MyCanvas myCanvas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int[][] testLevelMap = {
                {1,1,1,1,1,1,1,1,1,1},
                {1,1,1,1,1,1,1,1,1,1},
                {1,1,0,0,0,0,0,0,1,1},
                {1,1,0,0,0,0,0,0,1,1},
                {1,1,0,2,1,1,3,0,1,1},
                {1,1,0,0,1,1,0,0,1,1},
                {1,1,1,1,1,1,1,1,1,1},
                {1,1,1,1,1,1,1,1,1,1}
        };

        Level testLevel = new Level(testLevelMap, this.sideSize);

        myCanvas = new MyCanvas(this, testLevel);
        setContentView(myCanvas);

    }
}