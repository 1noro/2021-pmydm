package net.a3do.app.findyourself.model;

import android.graphics.Color;

public class Wall extends Block {

    public Wall(int x, int y, int sideSize) {
        super(x, y, sideSize);
        super.setColor(Color.BLUE);
    }

}
