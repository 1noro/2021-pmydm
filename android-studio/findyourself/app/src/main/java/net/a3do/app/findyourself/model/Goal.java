package net.a3do.app.findyourself.model;

import android.graphics.Color;

public class Goal extends Block {

    public Goal(int x, int y, int sideSize) {
        super(x, y, sideSize);
        super.setColor(Color.RED);
    }

}
