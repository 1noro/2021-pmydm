package com.example.buscaminascutre;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import java.util.Arrays;
import java.util.Random;

public class JuegoActivity extends AppCompatActivity {

    private int filas;
    private int columnas;
    private int minas;

    private int[][] arrayMinas;
    private int[][] arrayNumeros;
    private ImageView[][] arrayImageView;
    private int[][] arrayClicks;
    private int[][] arrayBanderas;

    private boolean juegoFinalizado = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        filas = intent.getIntExtra("filas", 8);
        columnas = intent.getIntExtra("columnas", 8);
        minas = intent.getIntExtra("minas", 10);

        setContentView(R.layout.activity_juego); // mostramos el XML

        Button buttonReset = findViewById(R.id.buttonReset);
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iniciarJuego();
            }
        });

        iniciarJuego();
    }

    public void iniciarJuego() {
        juegoFinalizado = false;

        calcularArrayMinas(); // hacer los cálculos
        calcularArrayNumeros(); // hacer los cálculos

        // llenamos el array de clicados con 0s
        arrayClicks = new int[filas][columnas];
        for (int f = 0; f < filas; f++) {
            for (int c = 0; c < columnas; c++) {
                arrayClicks[f][c] = 0;
            }
        }

        // llenamos el array de banderas con 0s
        arrayBanderas = new int[filas][columnas];
        for (int f = 0; f < filas; f++) {
            for (int c = 0; c < columnas; c++) {
                arrayBanderas[f][c] = 0;
            }
        }

        arrayImageView = new ImageView[filas][columnas]; // iniciamos el tamaño del array de ImageViews

        presentarTablero(); // mostramos el tablero
    }

    public void calcularArrayMinas() {
        arrayMinas = new int[filas][columnas];
        // llenamos la matriz de 0
        for (int f = 0; f < filas; f++) {
            for (int c = 0; c < columnas; c++) {
                arrayMinas[f][c] = 0;
            }
        }

        // llenar la matriz de tantos 1 como minas tenga el nivel
        int minasDesplegadas = 0;
        for (int f = 0; f < filas; f++) {
            for (int c = 0; c < columnas; c++) {
                arrayMinas[f][c] = 1;
                minasDesplegadas++;
                if (minasDesplegadas >= minas) {
                    break;
                }
            }
            if (minasDesplegadas >= minas) {
                break;
            }
        }
//        printBiArray(arrayMinas);
        bidimensionalArrayShuffle(arrayMinas);
        printBiArray(arrayMinas);
    }

    public void calcularArrayNumeros() {
        arrayNumeros = new int[filas][columnas];
        for (int f = 0; f < filas; f++) {
            for (int c = 0; c < columnas; c++) {
                arrayNumeros[f][c] = 0;
            }
        }

        for (int f = 0; f < filas; f++) {
            for (int c = 0; c < columnas; c++) {
                if (arrayMinas[f][c] != 1) {
                    try {
                        arrayNumeros[f][c] += arrayMinas[f-1][c-1];
                    } catch (IndexOutOfBoundsException ignored) {}
                    try {
                        arrayNumeros[f][c] += arrayMinas[f-1][c];
                    } catch (IndexOutOfBoundsException ignored) {}
                    try {
                        arrayNumeros[f][c] += arrayMinas[f-1][c+1];
                    } catch (IndexOutOfBoundsException ignored) {}
                    try {
                        arrayNumeros[f][c] += arrayMinas[f][c-1];
                    } catch (IndexOutOfBoundsException ignored) {}
                    try {
                        arrayNumeros[f][c] += arrayMinas[f][c+1];
                    } catch (IndexOutOfBoundsException ignored) {}
                    try {
                        arrayNumeros[f][c] += arrayMinas[f+1][c-1];
                    } catch (IndexOutOfBoundsException ignored) {}
                    try {
                        arrayNumeros[f][c] += arrayMinas[f+1][c];
                    } catch (IndexOutOfBoundsException ignored) {}
                    try {
                        arrayNumeros[f][c] += arrayMinas[f+1][c+1];
                    } catch (IndexOutOfBoundsException ignored) {}
                }
            }
        }
        printBiArray(arrayNumeros);
    }

    public void printBiArray(int[][] biArray) {
        StringBuilder stringOut = new StringBuilder("biArray\n");
        for (int[] row : biArray)
            stringOut.append(Arrays.toString(row)).append("\n");
        Log.d("Info", stringOut.toString());
    }

    public void presentarTablero() {
        // Creamos el grid
        int anchoPantalla = Resources.getSystem().getDisplayMetrics().widthPixels;
        int buttonSize = anchoPantalla / columnas;

        LinearLayout filasGrid = findViewById(R.id.filasGrid);
        filasGrid.removeAllViews(); // para cuando hagamos el reset (elimina todos los hijos de este layout)

        for (int f = 0; f < filas; f++) {
            LinearLayout linearLayoutFila = new LinearLayout(this);
            linearLayoutFila.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            ));
            linearLayoutFila.setOrientation(LinearLayout.HORIZONTAL);
            for (int c = 0; c < columnas; c++) {
                ImageView imageView = new ImageView(this);
                imageView.setLayoutParams(new LinearLayout.LayoutParams(buttonSize, buttonSize));
                imageView.setContentDescription(f + "x" + c);
                imageView.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.default_button));
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!juegoFinalizado) {
                            ImageView imageViewCapturada = (ImageView) view;
                            int f = Integer.parseInt(imageViewCapturada.getContentDescription().toString().split("x")[0]);
                            int c = Integer.parseInt(imageViewCapturada.getContentDescription().toString().split("x")[1]);
                            if (arrayClicks[f][c] == 0 && arrayBanderas[f][c] == 0) {
                                if (arrayMinas[f][c] == 1) {
                                    // es una mina
                                    imageViewCapturada.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.clicked_button_m));
                                    mostrarMinas();
                                    juegoFinalizado = true;
                                    Toast.makeText(getApplicationContext(), "PERDISTE", Toast.LENGTH_SHORT).show();
                                    Vibrator v = (Vibrator) getApplicationContext().getSystemService(getApplicationContext().VIBRATOR_SERVICE);
                                    v.vibrate(400);
                                } else {
                                    // es un número
                                    mostrarNumero(imageViewCapturada, f, c);
                                }
                                comprobarFinal();
                            }
                        }
                    }
                });
                imageView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        if (!juegoFinalizado) {
                            ImageView imageViewCapturada = (ImageView) view;
                            int f = Integer.parseInt(imageViewCapturada.getContentDescription().toString().split("x")[0]);
                            int c = Integer.parseInt(imageViewCapturada.getContentDescription().toString().split("x")[1]);
                            if (arrayClicks[f][c] == 0) {
                                if (arrayBanderas[f][c] == 0) {
                                    // ponemos bandera
                                    arrayBanderas[f][c] = 1;
                                    imageViewCapturada.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.marked_button));
                                } else {
                                    // quitamos bandera
                                    arrayBanderas[f][c] = 0;
                                    imageViewCapturada.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.default_button));
                                }
                            }
                        }
                        return true;
                    }
                });
                arrayImageView[f][c] = imageView;
                linearLayoutFila.addView(imageView);
            }
            filasGrid.addView(linearLayoutFila);
        }
    }

    public void mostrarNumero(ImageView imageView, int f, int c) {
        if (arrayClicks[f][c] == 0 && arrayBanderas[f][c] == 0) {
            arrayClicks[f][c] = 1;
            switch (arrayNumeros[f][c]) {
                case 0:
                    expansionDelCero(imageView, f, c);
                    break;
                case 1:
                    imageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.clicked_button_1));
                    break;
                case 2:
                    imageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.clicked_button_2));
                    break;
                case 3:
                    imageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.clicked_button_3));
                    break;
                case 4:
                    imageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.clicked_button_4));
                    break;
                case 5:
                    imageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.clicked_button_5));
                    break;
                case 6:
                    imageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.clicked_button_6));
                    break;
                case 7:
                    imageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.clicked_button_7));
                    break;
                case 8:
                    imageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.clicked_button_8));
                    break;
            }
        }
    }

    public void expansionDelCero(ImageView imageView, int f, int c) {
        imageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.clicked_button));
        try {
            mostrarNumero(arrayImageView[f - 1][c - 1], f - 1, c - 1);
        } catch (IndexOutOfBoundsException ignored) {}
        try {
            mostrarNumero(arrayImageView[f - 1][c], f - 1, c);
        } catch (IndexOutOfBoundsException ignored) {}
        try {
            mostrarNumero(arrayImageView[f - 1][c + 1], f - 1, c + 1);
        } catch (IndexOutOfBoundsException ignored) {}
        try {
            mostrarNumero(arrayImageView[f][c - 1], f, c - 1);
        } catch (IndexOutOfBoundsException ignored) {}
        try {
            mostrarNumero(arrayImageView[f][c + 1], f, c + 1);
        } catch (IndexOutOfBoundsException ignored) {}
        try {
            mostrarNumero(arrayImageView[f + 1][c - 1], f + 1, c - 1);
        } catch (IndexOutOfBoundsException ignored) {}
        try {
            mostrarNumero(arrayImageView[f + 1][c], f + 1, c);
        } catch (IndexOutOfBoundsException ignored) {}
        try {
            mostrarNumero(arrayImageView[f + 1][c + 1], f + 1, c + 1);
        } catch (IndexOutOfBoundsException ignored) {}
    }

    public static void bidimensionalArrayShuffle(int[][] a) {
        Random random = new Random();
        for (int i = a.length - 1; i > 0; i--) {
            for (int j = a[i].length - 1; j > 0; j--) {
                int m = random.nextInt(i + 1);
                int n = random.nextInt(j + 1);

                int temp = a[i][j];
                a[i][j] = a[m][n];
                a[m][n] = temp;
            }
        }
    }

    public void comprobarFinal() {
        boolean finalDetectado = true;
        for (int f = 0; f < filas; f++) {
            for (int c = 0; c < columnas; c++) {
                if (arrayMinas[f][c] == arrayClicks[f][c]) {
                    finalDetectado = false;
                    break;
                }
            }
            if (!finalDetectado) {
                break;
            }
        }
        if (finalDetectado) {
            juegoFinalizado = true;
            Toast.makeText(this, "GANASTE", Toast.LENGTH_SHORT).show();
        }
    }

    public void mostrarMinas() {
        for (int f = 0; f < filas; f++) {
            for (int c = 0; c < columnas; c++) {
                if (arrayMinas[f][c] == 1) {
                    arrayImageView[f][c].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.clicked_button_m));
                }
            }
        }
    }
}
