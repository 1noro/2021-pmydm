package com.example.buscaminascutre;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); // mostrar la pantalla definida en el XML

        Button buttonFacil = findViewById(R.id.buttonFacil);
        buttonFacil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), JuegoActivity.class);
                intent.putExtra("filas", 8);
                intent.putExtra("columnas", 8);
                intent.putExtra("minas", 10);
                startActivity(intent);
            }
        });

        Button buttonMedio = findViewById(R.id.buttonMedio);
        buttonMedio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), JuegoActivity.class);
                intent.putExtra("filas", 16);
                intent.putExtra("columnas", 16);
                intent.putExtra("minas", 40);
                startActivity(intent);
            }
        });
    }
}