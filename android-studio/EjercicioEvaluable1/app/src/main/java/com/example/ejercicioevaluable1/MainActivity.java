package com.example.ejercicioevaluable1;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private int listaMaxLenght = 100;
    private Integer[] lista = new Integer[this.listaMaxLenght];
    private int ultimaPosicionCalculada = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText entrada = findViewById(R.id.input);
        entrada.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE)
                    pulsarCalcular(entrada);
                return false;
            }
        });
    }

    public void pulsarCalcular(View view) {
        // no has introducdo nada
        // introduce una posición valida
        // si ya esta calculado en el array: se muestra
        // si no esta calculado previamente: se calcula hasta esa posición y se muestra
        // no has introducido una posicion valida

        EditText entrada = findViewById(R.id.input);
        TextView resultado = findViewById(R.id.resultado);

        if (entrada.getText().toString().trim().length() == 0) {
            resultado.setText(R.string.ningunaPosicionIntroducida);
        } else {
            int posicionIntroducida = Integer.parseInt(entrada.getText().toString()) - 1;
            if (posicionIntroducida >= 0 && posicionIntroducida < this.listaMaxLenght) {
                if (this.lista[posicionIntroducida] == null) {
                    Calcular.calcularListaHasta(this.lista, this.ultimaPosicionCalculada, posicionIntroducida);
                    this.ultimaPosicionCalculada = posicionIntroducida;
                }
                resultado.setText(String.valueOf(this.lista[posicionIntroducida]));
            } else {
                resultado.setText(R.string.posicionNoValida);
            }
        }

        entrada.clearFocus();
        hideKeyboard(this);
    }

    public static void hideKeyboard(Activity activity) {
        View view = activity.findViewById(android.R.id.content);
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}