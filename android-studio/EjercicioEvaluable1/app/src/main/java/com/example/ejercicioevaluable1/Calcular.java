package com.example.ejercicioevaluable1;

import android.util.Log;

import java.util.Arrays;

public class Calcular {

    public static int invertirNumero(int numero) {
        String numeroStr = String.valueOf(numero);
        StringBuilder numeroInvertidoStr = new StringBuilder();
        for (int i = numeroStr.length() - 1; i >= 0; i--) {
            numeroInvertidoStr.append(numeroStr.charAt(i));
        }
        return Integer.parseInt(numeroInvertidoStr.toString());
    }

    public static void calcularListaHasta(Integer[] lista, int ultimaPosicionCalculada, int posicionHasta) {
        int posicion = ultimaPosicionCalculada;
//        Log.d("Lista antes: ", Arrays.toString(lista));
        while (posicion <= posicionHasta) {
            int numero = posicion + 1;
            lista[posicion] = invertirNumero(numero * numero);
//            Log.d("Cálculo","Calculado " + lista[posicion] + " en " + posicion + ".");
            posicion++;
        }
//        Log.d("Lista después: ", Arrays.toString(lista));
    }

}
