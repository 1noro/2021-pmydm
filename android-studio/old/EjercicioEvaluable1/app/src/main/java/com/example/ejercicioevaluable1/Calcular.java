package com.example.ejercicioevaluable1;

import android.util.Log;

import java.util.Arrays;

public class Calcular {

    public static int primeraPosicionVacia(int[] lista) {
        int i = 0;
        while (lista[i] != 0) i++;
        return i;
    }

    public static int invertirNumero(int numero) {
        String numeroStr = String.valueOf(numero);
        StringBuilder numeroInvertidoStr = new StringBuilder();
        for (int i = numeroStr.length() - 1; i >= 0; i--) {
            numeroInvertidoStr.append(numeroStr.charAt(i));
        }
        return Integer.parseInt(numeroInvertidoStr.toString());
    }

    public static void calcularListaHasta(int[] lista, int posicion) {
//        Log.d("Lista antes: ", Arrays.toString(lista));
        for (int i = primeraPosicionVacia(lista); i <= posicion; i++) {
            int numero = i + 1;
            lista[i] = invertirNumero(numero * numero);
//            Log.d("Cálculo","Calculado " + lista[i] + " en " + i + ".");
        }
//        Log.d("Lista después: ", Arrays.toString(lista));
    }

}
