package com.example.ejercicioevaluable1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int listaMaxLenght = 100;
    int[] lista = new int[this.listaMaxLenght];

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        for (int i = 0; i < this.lista.length; i++) {
            lista[i] = 0;
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void pulsarCalcular(View view) {
        // no has introducdo nada
        // introduce una posición valida
        // si ya esta calculado en el array: se muestra
        // si no esta calculado previamente: se calcula hasta esa posición y se muestra
        // no has introducido una posicion valida

        LinearLayout mainLayout = (LinearLayout) findViewById(R.id.mainLayout);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mainLayout.getWindowToken(), 0);

        EditText entrada = findViewById(R.id.input);
        TextView resultado = (TextView) findViewById(R.id.resultado);

        if (entrada.getText().toString().trim().length() == 0) {
            resultado.setText(R.string.ninguanPosicionIntroducida);
        } else {
            int posicion = Integer.parseInt(entrada.getText().toString()) - 1;
            if (posicion >= 0 && posicion < this.listaMaxLenght) {
                if (this.lista[posicion] == 0) {
                    Calcular.calcularListaHasta(this.lista, posicion);
                }
                resultado.setText(String.valueOf(this.lista[posicion]));
//                Toast.makeText(this, String.valueOf(this.lista[2]), Toast.LENGTH_SHORT).show();
            } else {
                resultado.setText(R.string.posicionNoValida);
            }
        }

    }

}